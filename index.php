<?php
// remplacer http par https
define('URL_BASE', (isset($_SERVER['HTTPS']) ? 'https' : 'http').'://'.$_SERVER['HTTP_HOST'].str_replace($_SERVER['DOCUMENT_ROOT'], '', dirname($_SERVER['SCRIPT_FILENAME'])));
define('TITLE', 'MyFrama');

/**
 * Build a directory path by concatenating a list of directory names.
 *
 * You can call this function with as many params as you want.
 * Example: join_path('home', 'test', 'Downloads') returns home/test/Downloads
 *
 * @param string [...] a list of directory names
 * @return a string corresponding to the final pathname
 */
function join_path() {
    $path_parts = func_get_args();
    return join(DIRECTORY_SEPARATOR, $path_parts);
}

define('PATH_ROOT', dirname(__FILE__));
define('PATH_ACCOUNTS', join_path(PATH_ROOT, 'u'));
define('PATH_SHAARLI', join_path(PATH_ROOT, 'shaarli'));

// Init plugins, template and users folders
if ($handle = opendir('plugins')) {
    while (false !== ($file = readdir($handle))) {
        if (!file_exists(join_path(PATH_SHAARLI, 'plugins', $file))) {
            symlink( '../../plugins/' . $file, 'shaarli/plugins/' . $file);
        }
    }
    closedir($handle);
}

if(!file_exists(PATH_ACCOUNTS)) {
    $folder = mkdir('u');
    if(!$folder){
        echo 'Erreur : problème de permissions pour créer le dossier u';
    }
    chmod('u' , 0755);
}
if(file_exists(join_path(PATH_SHAARLI, 'data'))) {
    chmod('shaarli/data' , 0500);
}

require_once( 'setconfig.php');

// Dirty thing to call locale files from 'i18n' and 'myframa' plugins
$GLOBALS['config']['ENABLED_PLUGINS'] = array(
      0 => 'i18n',
      1 => 'myframa'
);

if(file_exists(PATH_SHAARLI.'/plugins/i18n/i18n.php')){
    require_once( PATH_SHAARLI.'/plugins/i18n/i18n.php');
} else {
    echo 'Erreur : Plugin i18n requis dans '.PATH_SHAARLI.'/plugins/i18n/i18n.php';
}


/**
 * Test if input values are correct.
 *
 * Tested values are:
 *  - account: max 25 chars from [a-z0-9_-]{1,25}
 *  - password: not empty
 *  - email: valid email address
 * All values are required.
 *
 * @param array $values input values to validate.
 * @return true if $values are correct, false otherwise.
 */
function values_are_valid($values) {
    if ( empty($values['account'])
        || empty($values['password'])
        || empty($values['email'])) {
        return 'empty';
    }

    if (!preg_match('/^[a-z0-9_-]{1,25}?$/', strtolower($values['account']))) {
        return 'account';
    }

    if (strlen($values['password']) <= 0) {
        // Already tested by empty(...) but if one day we want to set
        // a minimal number of chars, we'll just have to change the 0.
        return 'password';
    }

    return true;
}

/**
 * Check if an account is already existing.
 *
 * @param string $account the name of the subdomain to check.
 * @return bool True if the account exists, false otherwise.
 */
function check_account_exists($account) {
    $account_path = join_path(PATH_ACCOUNTS, $account);
    if (file_exists($account_path)) {
        return true;
    } else {
        return false;
    }
}

/**
 * Create a new account.
 *
 * This function creates files under PATH_ACCOUNTS and set the database with
 * correct values.
 * Note values must be verified before calling this function.
 *
 * @param string $account name of the account to create.
 * @param string $password the account password.
 * @param string $email an email address.
 * @throws Exception if any error occured during process.
 */
function create_account($account, $password, $email) {
    $account_path = join_path(PATH_ACCOUNTS, strtolower($account));
    mkdir($account_path);

    $userData    = array('data', 'tmp', 'cache', 'pagecache');
    $blacklist   = array('.', '..', '.git',);

    // Relative symlink to shaarli's content
    if ($handle = opendir('shaarli')) {
        $blacklist = array_merge($blacklist, $userData);

        while (false !== ($file = readdir($handle))) {
            if (!in_array($file, $blacklist)) {
                symlink( '../../shaarli/' . $file, 'u/' . $account . '/' . $file);
            }
        }
        closedir($handle);
    }

    // Create userData folders
    foreach ($userData as $file) {
        mkdir($account_path . '/' . $file);
        chmod('u/' . $account . '/' . $file , 0755);
        copy('shaarli/' . $file . '/.htaccess', 'u/' . $account . '/' . $file . '/.htaccess');
        copy('shaarli/plugins/myframa/datastore.php', 'u/' . $account . '/data/datastore.php');
    }

    //User parameters
    $config['login'] = $account;
    $config['salt'] = sha1(uniqid('',true).'_'.mt_rand()); // Salt renders rainbow-tables attacks useless.
    $config['hash'] = sha1($password.$config['login'].$config['salt']);

    //Generate config file
    writeConfig($config, true, $account_path . '/data/config.json.php');
}

//------------------------------------------------------------------
// ~~~~~~~~~~~~~~~~~~~~~~ 0. Unset Cookie ~~~~~~~~~~~~~~~~~~~~~~~~~~
if(isset($_GET['cookie']) && $_GET['cookie']=='unset') {
    setcookie('myframa', '', time()+365*24*60*60, dirname($_SERVER["SCRIPT_NAME"]).'/');
    $_COOKIE['myframa'] = '';
}

// ~~~~~~~~~~~~~~~~~~~~~~ 1. Bookmarklet ~~~~~~~~~~~~~~~~~~~~~~~~~~~
function encodeURIComponent($str) {
    $revert = array('%21'=>'!', '%2A'=>'*', '%27'=>"'", '%28'=>'(', '%29'=>')');
    return strtr(rawurlencode($str), $revert);
}

$bm_src= ''; $bm_post = '';
if (isset($_GET['source']) && $_GET['source']=='bookmarklet') {
    $bm_src = '&source=bookmarklet';
    if (isset($_GET['post'])) {
        $bm_url = encodeURIComponent($_GET['post']);
        $bm_title = isset($_GET['title']) ? encodeURIComponent($_GET['title']) : '';
        $bm_description = isset($_GET['description']) ? encodeURIComponent($_GET['description']) : '';
        $bm_post = '&post='.$bm_url.'&title='.$bm_title.'&description='.$bm_description;
    }
    if(isset($_COOKIE['myframa']) && !empty($_COOKIE['myframa'])) {
        $account = $_COOKIE['myframa'];
        header('Location: u/' . $account . '?'.$bm_post.$bm_src);
    }
}

// ~~~~~~~~~~~~~~~~~~~~~~ 2. Account ~~~~~~~~~~~~~~~~~~~~~~~~~~~
$test_values = true;
if (!is_writable(PATH_ACCOUNTS)) {

    $error = t('The directory containing the data must be writable by the web server.');

} elseif (isset($_POST['subdomain_redirect'])) {
    // Handle account redirection.
    $account = strtolower($_POST['subdomain_redirect']);
    if (check_account_exists($account)) {
        // Keep user account in a cookie
        setcookie('myframa', $account, time()+365*24*60*60, dirname($_SERVER["SCRIPT_NAME"]).'/');
        // Redirection
        header('Location: u/' . $account . '?do=login'.$bm_post.$bm_src );
    } else {
        $error_redirection = t('This account does not seem to exist. Are you sure of your account name?');
    }

} elseif (!empty($_POST)) {
    $test_values = values_are_valid($_POST);

    $account_exists = true;
    $error = null;
    $account = strtolower($_POST['account']);
    $password = $_POST['password'];
    $email = $_POST['email'];

    if ($test_values === true) {
        try {
            $account_exists = check_account_exists($account);
        } catch (Exception $e) {
            $error = $e->getMessage();
        }
    } else {
        $error = t('Please check the information you entered matches the requested format.');
    }

    if ($account_exists === false) {
        // If $account_exists is false, there was no error catched earlier.
        try {
            create_account($account, $password, $email);
        } catch (Exception $e) {
            $error = $e->getMessage();
        }
    }

    if (is_null($error) && $account_exists) {
        $error = t('This account appears to already exist, please choose another account name.');
        $test_values = 'account';
    } elseif (is_null($error)) {
        // Keep user account in a cookie
        setcookie('myframa', $account, time()+365*24*60*60, dirname($_SERVER["SCRIPT_NAME"]).'/');
        // Everything is fine, we can redirect user to its new account!
        header('Location: u/' . $account . '?do=login&first'.$bm_post.$bm_src);
    }
}

/*$out = unserialize(gzinflate(base64_decode('1Vddchy3EfbzngJevogMub/kWhxWKkXTsmOXLSkSJUfFuETsTO8uvBhgAmC4WknUFXKCvOUQOY8v4Cv4a2BmdyNLKsUP+WEVi5hGo38+dH9oPsjGg6z7eCGl0+ovn1u7LKVbbhbnzsl1NzvJXvlsOMq6nxx8Mm22fPdMZuPslcoGZw+y0ckHrHT5LCycRAOq6J6pbHQGi8Mo8AvrwhOnuxBNsu5fPz95Nlk1H9iu087wOOv2Pbug/lblblQJKmiKn6NNGOJIXC5IVOS8NVIfilIZVUqtfDgUvob8aCZ5Xcggp9KTmDmiQ9Hmp8xceHI3KqfWPTwV5HOnqqCsif5OkdP3pHNbkghWNL4/7XTaKKTWduXF2ta83xrnbydm8sY6FRCknJM/FNIUAhk6mFpQKVYqLITF0nlhnfDBtjuVUzcykF73Os9gOJdGyKIQUuyEx+6il82Fcdr5QkgPIwomDSWXQc6j2V6nc+EIdmHI0EoksMV0LXKt8ggI1MT172JudC2mdQjWHHJwtedtadbCzqKWI2BSkikIDqzVXtyZOiBBTtCLQMYrPlnaqdLwV1Vb4DXhUh7de3wpzh9+fSgo5L19hNYmStIrvYb94BTdUMpRK8P54dsk2MLC1h7J+Sae8rAJkWNTJpBDTeB+pQMiZOaKwWCQkyQsnK3nC4YGgVNv3kuYpcwFVtiZI7Xo7Nqny76OcF4vSFfXCPmP0i+igRb2vUZP7LGKYEtSe8vVWKEFqNi5TpbPlEagMWR5I5WWU2B19ejxYzEjKn640+c/fRlsuR89VyoPNYyuUHR8bXyxyKnSkjN+EZrsAOb3JBa24mqSIdbmSuEImR/tusGpqV9cER/Gb5vsT3//x8///Fs0j7j4kmujwvrTzpdEOvYQVx6MG3GFRJT3Nf1wZxFC5bN+fw4T9bSHY/0GjH7jqR81/b5QsxjRQt5wIfoaOPtY0EiFTG5rvj3Rmu51uA8/SzQAsJmVRomVorjxwpQzhARdzNh3z243hIaAyqkBvN2z+9A4TZwUUPHrJBkOoiiPvQHyesCs8wXWl6qkLnNgtAseiUwxAm2NBqPh0WB4NBqJ4SQ7mWTj097p6cnx3UnkknHWDTj7Ei34PKwr4ujGic1aedRDgPdqByz7D6VTPkWdoqmrIkWzE2FDC0k2SjLwgmL0pL6wAM4EBmiQvbq9ZUB+A3EP30vc+eV8ejL/IHFvVd4ibg7jvg2UtS22w2T+A/w7gp1zpMjk0xIWl6mtA4oFhZJ2njz6VqT78/jiXjiyBizSNfDZFZX14Vfk2Otc8oo1uOEbcEGikVdFQ9Mi2oG6iN2J0vfoABXEasHMpm2kCWV2GKz2lJxAeUlURRdgrxiFdKg8zZ+5LUh4o6qKQvM4lBxiiWcA1vhx+w53VNiVETPrShlCerPS3399eJJCIsvoLtrbPEc7sGadzt7enrjkmxELkoytj7I98V2tg6p25EKDcrXvCHGA10CDugU/sEnwXAU8t/nziHeUHBxMrS4ODraSN288eHxJLeO+ebPdu2YE8Mpomy+TRbzfIIO4vIqUvyUWMr2VWqqKCiV71s37/NVvAdrvdDZY7VIu8ziuzSPp1+K+xCOOn9fiEj0pRFpfWA3iweJPYc0CKB6lH7FZ7azbRVR84KSZEx/+0tWoiWSxkb4Ww9GksXheMapvK56bdVpMRqJR/JZKMOHbis+ILxuL8aBVvJDO2cCbT2lOMUusH6EW44nhcVL8fyLPyd3/CHlOY64fQZ+j30Kfg/fS5wv1Z3n8a/o8Rr5tka9Wqx5aONRTik/oSoZ88Yeb33/x9N6TfPpwOT3K38GtE+B7IXUpvpEvX6KHvcoxHIONLmFH3GkS35JBM0A1FwtuKO3++yl4zObbaTeOEbLGRCJRA6CgNQ8lS1FXkSu3RsFHaWhjcpLiBjdEIQ6PK5p6zMQeFHfvRaV55k2shYm0daOMD9LkcTYNbs10x4Sf44ocj5qRK8nUMPJUwVp0Xjn7I+UBXC2uvopjyMeMJfs8dlzx+cLmNYwGyZlvjzb6PRR4ATVo+Z6y4KN+iX8vyPX3OUWNyctE6sY7wbE25pHlfXTuuh1uUW3ssCBQaXpfCppJ0G7zuPl3zTvjj2jZ9B8X+nOnY/87bTwc/y+18W3bbqrYYjmKjcoIpt9BhO9207l4qVWq/w3ukC5p7Vu7rQxN3Jp997+wDSO8Y0ZqLvDfbf8Y6e0v')));
echo '<pre>';
var_dump($out);
echo '</pre>';*/

include 'layout.php';
