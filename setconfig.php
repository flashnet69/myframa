<?php
//-- From shaarli/index.php --------------------------------------------

$datadir = 'data';

$GLOBALS['config']['resources']['data_dir'] = 'data';
$GLOBALS['config']['resources']['config'] = $datadir.'/config.json.php';
$GLOBALS['config']['resources']['datastore'] = $datadir.'/datastore.php';
$GLOBALS['config']['resources']['ban_file'] = $datadir.'/ipbans.txt';
$GLOBALS['config']['resources']['updates'] = $datadir.'/updates.txt';
$GLOBALS['config']['resources']['log'] = $datadir.'/log.txt';
$GLOBALS['config']['resources']['update_check'] = $datadir.'/lastupdatecheck.txt';
$GLOBALS['config']['resources']['history'] = $datadir.'/history.php';
$GLOBALS['config']['resources']['raintpl_tpl'] = 'tpl/';
$GLOBALS['config']['resources']['theme'] = 'default';
$GLOBALS['config']['resources']['raintpl_tmp'] = 'tmp/';
$GLOBALS['config']['resources']['thumbnails_cache'] = 'cache';
$GLOBALS['config']['resources']['page_cache'] = 'pagecache';

$GLOBALS['config']['security']['ban_after'] = 4;
$GLOBALS['config']['security']['ban_duration'] = 1800;
$GLOBALS['config']['security']['session_protection_disabled'] = false;
$GLOBALS['config']['security']['allowed_protocols'] = array('ftp', 'ftps', 'magnet');
//$GLOBALS['config']['security']['trusted_proxies'] = array('1.2.3.4', '5.6.7.8');

$GLOBALS['config']['general']['header_link'] = 'myframa/shaarli/';
$GLOBALS['config']['general']['links_per_page'] = 20;
$GLOBALS['config']['general']['default_note_title'] = 'Note: ';
$GLOBALS['config']['general']['retrieve_description'] = true;
$GLOBALS['config']['general']['enable_async_metadata'] = true;
$GLOBALS['config']['general']['timezone'] = 'Europe/Paris';
$GLOBALS['config']['general']['title'] = 'My Shaarli';

$GLOBALS['config']['feed']['rss_permalinks'] = true;
$GLOBALS['config']['feed']['show_atom'] = false;

$GLOBALS['config']['privacy']['default_private_links'] = true;
$GLOBALS['config']['privacy']['hide_public_links'] = false;
$GLOBALS['config']['privacy']['force_login'] = false;
$GLOBALS['config']['privacy']['hide_timestamps'] = false;
$GLOBALS['config']['privacy']['remember_user_default'] = true;

$GLOBALS['config']['dev']['debug'] = false;

$GLOBALS['config']['extras']['hide_public_links'] = false;
$GLOBALS['config']['extras']['hide_timestamps'] = false;
$GLOBALS['config']['extras']['open_shaarli'] = false;

$GLOBALS['config']['updates']['check_updates'] = true;
$GLOBALS['config']['updates']['check_updates_branch'] = 'latest';
$GLOBALS['config']['updates']['check_updates_interval'] = 86400;

$GLOBALS['config']['thumbnail']['enable_thumbnails'] = true;
$GLOBALS['config']['thumbnail']['enable_localcache'] = true;

$GLOBALS['config']['plugins'] = array();

$GLOBALS['config']['translation']['language'] = 'fr';
$GLOBALS['config']['translation']['mode'] = 'php';
$GLOBALS['config']['translation']['extensions']['demo'] = 'plugins/demo_plugin/languages/';

$GLOBALS['config']['formatter'] = 'markdown';

$GLOBALS['config']['api']['enabled'] = true;
$GLOBALS['config']['api']['secret'] = sha1(uniqid('',true).'_'.mt_rand());


/*
 * Plugin configuration
 *
 * Warning: order matters!
 *
 * These settings may be be overriden in:
 *  - data/config.json.php
 *  - each plugin's configuration file
 */
$GLOBALS['config']['general']['enabled_plugins'] = array('qrcode');

/**
 * Re-write configuration file according to given array.
 * Requires mandatory fields listed in $MANDATORY_FIELDS.
 *
 * @param array $config     contains all configuration fields.
 * @param bool  $isLoggedIn true if user is logged in.
 *
 * @return void
 *
 * @throws MissingFieldConfigException: a mandatory field has not been provided in $config.
 * @throws UnauthorizedConfigException: user is not authorize to change configuration.
 * @throws Exception: an error occured while writing the new config file.
 */
function writeConfig($config, $isLoggedIn, $config_file)
{
    // These fields are required in configuration.
    $MANDATORY_FIELDS = array(
        'login', 'hash', 'salt'
    );

    if (!isset($config_file)) {
        throw new MissingFieldConfigException('CONFIG_FILE');
    }

    // Only logged in user can alter config.
    if (is_file($config_file) && !$isLoggedIn) {
        throw new UnauthorizedConfigException();
    }

    // Check that all mandatory fields are provided in $config.
    foreach ($MANDATORY_FIELDS as $field) {
        if (!isset($config[$field])) {
            throw new MissingFieldConfigException($field);
        }
    }

    $GLOBALS['config']['credentials']['login'] = $config['login'];
    $GLOBALS['config']['credentials']['hash']  = $config['hash'];
    $GLOBALS['config']['credentials']['salt']  = $config['salt'];

    // Generate JSON file
    $configStr = '<?php /*'. PHP_EOL;
    $configStr .= json_encode($GLOBALS['config'], JSON_PRETTY_PRINT);
    $configStr .= '*/ ?>';

    if (!file_put_contents($config_file, $configStr)
        || strcmp(file_get_contents($config_file), $configStr) != 0
    ) {
        throw new Exception(
            'Shaarli could not create the config file.
            Please make sure Shaarli has the right to write in the folder is it installed in.'
        );
    }
}
